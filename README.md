## 项目描述

本项目是一个书法字体风格识别器，通过输入图片，识别出图片中的书法字体风格。项目包含以下文件：
- `0_setting.yaml`：配置文件，包含书法字体风格列表、图片调整大小的目标尺寸等设置。
- `1_Xy.py`：预处理图像、生成训练和测试数据集。
- `2_fit.py`：使用LazyClassifier评估多个分类模型，选择F1分数最高的模型并保存。
- `3_predict.py`：创建一个简单的图形用户界面，用户可以选择图像，程序会显示预测的书法字体风格。
- `util.py`：包含一些辅助功能，例如图像预处理、保存和加载文件等。

## 项目运行效果截图
![输入图片说明](shufa1.png)
![输入图片说明](shufa3.png)
![输入图片说明](shufa2.png)
![输入图片说明](shufa4.png)
![输入图片说明](shufa5.png)
![输入图片说明](shufa6.png)

## 功能

1. 预处理图像并生成训练和测试数据集。
2. 使用LazyClassifier评估多个分类模型，选择F1分数最高的模型并保存。
3. 创建一个简单的图形用户界面，用户可以选择图像，程序会显示预测的书法字体风格。

## 依赖

- Python
- Scikit-learn
- LazyPredict
- OpenCV
- PIL
- Tkinter
- PyYAML

## 使用

1. 确保已安装所有依赖库。
2. 运行 `1_Xy.py` 生成训练和测试数据集。
3. 运行 `2_fit.py` 评估多个分类模型并保存最佳模型。
4. 运行 `3_predict.py` 启动图形用户界面，选择图像进行预测。

## 注意

- 在 wolai 作业页面下载书法字体文件 `shufa.zip`
- 请按照配置文件 `0_setting.yaml` 中的设置生成相关的文件夹，和放置文件位置。
- 请确保已安装所有依赖库。

## 个人信息
[TODO: 此处填写个人信息]
- 学号: 202152320104
- 年级: 2021
- 专业: 智能科学与技术
- 班级: 一 班
