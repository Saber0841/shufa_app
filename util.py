# 引入必要的库
import yaml
import time
from PIL import Image
import numpy as np
import joblib
import os
import time
# 获取 0_setting.yaml 中的键 key 对应的值 value
def get(key):
    # 读取 YAML 文件
    with open('0_setting.yaml', 'r', encoding='utf-8') as file:
        config = yaml.safe_load(file)
    
    # 获取指定键的值
    value = config.get(key)
    return value

# 预处理图像, 把图像设置为指定大小之后，展平返回
def preprocess_image(file_name, new_size):
    # 1. 读取图像灰度图
    img = Image.open(file_name).convert('L')
    # 2. 调整图像大小为 new_size
    img = img.resize(new_size)
    # 3. 将图像展平为一维数组
    img = np.array(img).flatten()
    return img



# 用joblib把叫做 name 的对象 obj 保存(序列化)到位置 loc
def dump(obj, name, loc):
    start = time.time()
    print(f"把{name}保存到{loc}") 
    joblib.dump(obj, loc)# 此处序列化对象
    end = time.time()
    print(f"保存完毕,文件位置:{loc}, 大小:{os.path.getsize(loc) / 1024 / 1024:.3f}M")
    print(f"运行时间:{end - start:.3f}秒")

# 用joblib读取(反序列化)位置loc的对象obj,对象名为name
def load(name, loc):
    print(f"从{loc}提取文件{name}")
    obj = joblib.load(loc)# 此处反序列化对象
    return obj